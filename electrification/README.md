# Electrification

The [electrification_exploration.ipynb](electrification_exploration.ipynb) notebook contains code that simulates a [generic air-water heat pump model](https://github.com/RE-Lab-Projects/hplib) and is used to assess electrification in 15 AHUs in `Building 452`.

# Usage
Install the requirements contained in [requirements.txt](requirements.txt):

```console
pip install -r requirements.txt
```

Run [electrification_exploration.ipynb](electrification_exploration.ipynb) to generate output figures. The dependent data files are [building_452_data.pkl](building_452_data.pkl) and [grid_data.pkl](grid_data.pkl) which contain `Building 452` sensible load and carbon intensity calculations for [NYISO](http://mis.nyiso.com/public/P-63list.htm) respectively. The output figures are written to [images/electrification](../images/electrification) in the parent directory.